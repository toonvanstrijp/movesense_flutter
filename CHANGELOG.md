## 0.0.1 - 2020-06-24

- initial release

- includes example app that connects to a movesense and gets its info

- Android is supported; iOS is not yet supported

- README includes instructions to put `aar` in mavenLocal
